package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fr_binome_07.ArcOut;
import fr_binome_07.PetriNetwork;
import fr_binome_07.Place;

public class ArcOutTest {
	private ArcOut a;

	@Before
	public void setUp() throws Exception {
		PetriNetwork.resetIds();
		Place p = new Place(10);
		this.a = new ArcOut(5, p);
	}

	@Test
	public void pullTest() throws Exception {
		this.a.pull();
		assertEquals(this.a.getPlace().getTokens(), 10+5);
	}

}
