package tests;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

import fr_binome_07.Arc;
import fr_binome_07.ArcEmpty;
import fr_binome_07.ArcIn;
import fr_binome_07.ArcOut;
import fr_binome_07.ArcZero;
import fr_binome_07.InvalidPullException;
import fr_binome_07.PetriNetwork;
import fr_binome_07.Place;
import fr_binome_07.Transition;

public class PetriNetworkTest {
	private PetriNetwork pn;

	@Before
	public void setUp() throws Exception {
		PetriNetwork.resetIds();
		pn = new PetriNetwork();
		Place pIn1 = new Place(7);
		Place pIn2 = new Place(0);
		Place pIn3 = new Place(52);
		Place pOut1 = new Place();
		Place pOut2 = new Place(3);
		ArcIn aIn = new ArcIn(2, pIn1);
		ArcZero aZero = new ArcZero(pIn2);
		ArcEmpty aEmpty = new ArcEmpty(pIn3);
		ArcOut aOut1 = new ArcOut(pOut1);
		ArcOut aOut2 = new ArcOut(27, pOut2);
		Transition t = new Transition(aIn, aZero, aEmpty, aOut1, aOut2);
		pn.addPlaces(pIn1, pIn2, pIn3, pOut1, pOut2);
		pn.addArcs(aIn, aZero, aEmpty, aOut1, aOut2);
		pn.addTransition(t);
	}
	
	@Test
	public void pullTest() throws InvalidPullException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		pn.pull(0);
		
		Transition t = pn.getTransition(0);
		Method privateMethod;
		privateMethod = Transition.class.getDeclaredMethod("getArcs");
		// Set the accessibility as true 
        privateMethod.setAccessible(true); 

        // Store the returned value of 
        // private methods in variable 
        ArrayList<Arc> arcs = (ArrayList<Arc>) privateMethod.invoke(t); 
        
        assertEquals(arcs.get(0).getPlace().getTokens(), 7-2);
        assertEquals(arcs.get(1).getPlace().getTokens(), 0);
        assertEquals(arcs.get(2).getPlace().getTokens(), 0);
        assertEquals(arcs.get(3).getPlace().getTokens(), 0);
        assertEquals(arcs.get(4).getPlace().getTokens(), 3+27);
	}
	
	@Test
	public void addPlaceTest() {
		Place p = new Place();
		pn.addPlace(p);
		assertTrue(pn.getPlaces().containsValue(p));
	}
	
	@Test
	public void addTransitionTest() throws Exception {
		Place p1 = new Place();
		Place p2 = new Place(451);
		Arc aIn = new ArcZero(p1);
		Arc aOut = new ArcOut(54, p2);
		Transition t = new Transition(aIn, aOut);
		pn.addTransition(t);
		assertTrue(pn.getTransitions().containsValue(t));
		assertTrue(pn.getArcs().containsValue(aIn));
		assertTrue(pn.getArcs().containsValue(aOut));
		assertTrue(pn.getPlaces().containsValue(p1));
		assertTrue(pn.getPlaces().containsValue(p2));
	}
	
	@Test
	public void addArcTest() throws Exception {
		Place p = new Place(64);
		Arc a = new ArcOut(p);
		pn.addArc(a);
		assertTrue(pn.getArcs().containsValue(a));
		assertTrue(pn.getPlaces().containsValue(p));
	}
	
	@Test
	public void removePlaceTest() {
		Place p = pn.removePlace(1);
		assertFalse(pn.getArcs().containsKey(p.getId()));
		assertEquals(pn.getArc(1), null);
	}
	
	@Test
	public void removeTransitionTest() {
		/*Place p = new Place(64);
		Arc a = new ArcOut(p);
		pn.addArc(a);*/
		pn.removeTransition(0);
		assertTrue(pn.getTransitions().isEmpty());
		assertTrue(pn.getArcs().isEmpty());
	}
	
	@Test
	public void removeArcTest() {
		Arc a = pn.removeArc(0);
		assertFalse(pn.getArcs().containsValue(a));
		assertFalse(pn.getTransition(0).getArcs().contains(a));
		assertTrue(pn.getPlaces().containsKey(a.getPlace().getId()));
	}
	
	@Test
	public void giveTokensTest() throws Exception {
		pn.giveTokens(3, 7);
		assertEquals(pn.getPlace(3).getTokens(), 0+7);
	}
	
	@Test
	public void takeTokensTest() throws Exception {
		pn.takeTokens(2, 24);
		assertEquals(pn.getPlace(2).getTokens(), 52-24);
	}
	
	@Test
	public void setArcValueTest() {
		pn.setArcValue(3, 985);
		assertEquals(pn.getArc(3).getValue(), 985);
	}
	

}
