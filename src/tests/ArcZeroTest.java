package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import fr_binome_07.ArcEmpty;
import fr_binome_07.ArcZero;
import fr_binome_07.InvalidPullException;
import fr_binome_07.PetriNetwork;
import fr_binome_07.Place;

public class ArcZeroTest {
	private ArcZero a;

	@Before
	public void setUp() throws Exception {
		PetriNetwork.resetIds();
		Place p = new Place();
		this.a = new ArcZero(p);
	}

	@Test
	public void isPullableTest() throws Exception {
		assertTrue(a.isPullable());
		a.getPlace().giveTokens(65);
		assertFalse(a.isPullable());
	}
	
	@Test
	public void pullTest() throws InvalidPullException {
		a.pull();
		assertEquals(a.getPlace().getTokens(), 0);
	}
}
