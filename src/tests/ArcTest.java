package tests;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr_binome_07.Arc;
import fr_binome_07.PetriNetwork;
import fr_binome_07.Place;
import fr_binome_07.Transition;

public class ArcTest {
	private Arc a;

	@Before
	public void setUp() throws Exception {
		PetriNetwork.resetIds();
		Place p = new Place(7);
		this.a = new Arc(5, p);
	}

	@Test
	public void arcTest() throws Exception {
		Place p = new Place(7);
		Method privateMethod;
		Class[] cArg = new Class[1];
        cArg[0] = int.class;
		privateMethod = Place.class.getDeclaredMethod("setId", cArg); //add parameterType
		// Set the accessibility as true 
        privateMethod.setAccessible(true); 
        // Store the returned value of 
        // private methods in variable 
        privateMethod.invoke(p, this.a.getPlace().getId()); 
		assertEquals(this.a.getPlace(), p);
		assertEquals(this.a.getValue(), 5);
	}
	
	@Test
	public void getValueTest() {
		assertEquals(this.a.getValue(), 5);
	}
	
	@Test
	public void setValueTest() {
		this.a.setValue(7);
		assertEquals(this.a.getValue(), 7);
	}
	
	@Test
	public void getPlaceTest() throws Exception {
		Place p = new Place(7);
		Method privateMethod;
		privateMethod = Place.class.getDeclaredMethod("setId", int.class); //add parameterType
		// Set the accessibility as true 
        privateMethod.setAccessible(true); 
        // Store the returned value of 
        // private methods in variable
        privateMethod.invoke(p, this.a.getPlace().getId()); 
		assertEquals(this.a.getPlace(), p);
	}
	
	@Test
	public void isPullableTest() {
		assertTrue(a.isPullable());
		a.setValue(9);
		assertFalse(a.isPullable());
	}
	
	@Test
	public void equalsTest() throws Exception {
		Place p = new Place(7);
		Arc arcSimilar1 = new Arc(5, p);
		Arc arcSimilar2 = new Arc(5, a.getPlace());		
		assertTrue(a.equals(a));
		assertFalse(a.equals(arcSimilar1));
		assertFalse(a.equals(arcSimilar2));
	}

}
