package tests;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import fr_binome_07.PetriNetwork;
import fr_binome_07.Place;

public class PlaceTest {

	@Before
	public void setUp() throws Exception {
		PetriNetwork.resetIds();
	}
	
	@Test
	public void validCreationTest() {
		Place p0 = new Place();
		assertEquals(p0.getTokens(), 0);
		Place p5;
		try {
			p5 = new Place(5);
			assertEquals(p5.getTokens(), 5);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getTokensTest() {
		Place p5;
		try {
			p5 = new Place(5);
			assertEquals(p5.getTokens(), 5);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void setTokens() {
		Place p = new Place();
		// Create Method object 
        Method privateMethod;
		try {
			privateMethod = Place.class.getDeclaredMethod("setTokens", int.class);
			// Set the accessibility as true 
	        privateMethod.setAccessible(true); 

	        // Store the returned value of 
	        // private methods in variable 
	        privateMethod.invoke(p, 5); 
	        assertEquals(p.getTokens(), 5);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	@Test
	public void giveTokensTest() {
		Place place;
		try {
			place = new Place(18);
			place.giveTokens(7);
			assertEquals(18+7, place.getTokens());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void takeTokensTest() {
		Place p;
		try {
			p = new Place(84);
			p.takeTokens(42);
			assertEquals(84-42, p.getTokens());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void equalsTest() {
		Place p1 = new Place();
		try {
			Place p2 = new Place(0);
			Place p3 = new Place(4);
			assertFalse(p1.equals(p2));
			assertFalse(p1.equals(p3));
			assertFalse(p2.equals(p3));
			assertTrue(p1.equals(p1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
