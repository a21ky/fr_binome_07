package fr_binome_07;

public class Arc {
	private int value;
	private Place place;
	private int id;
	private static int next_id;
	
	// can there be several arcs connecting one place and a transition ?
	public Arc(Place place) {
		this.setPlace(place);
		this.value = 0;
		this.id = next_id;
		incrementId();
	}
	public Arc(int value, Place place) {
		this(place);
		this.value = value;
	}
	
	private static void incrementId() {
		next_id += 1;
	}
	
	public static void resetId() {
		next_id = 0;
	}
	
	public int getId() {
		return this.id;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public Place getPlace() {
		return this.place;
	}
	
	private void setPlace(Place place) {
		this.place = place;
	}
	
	public boolean isPullable() {
		return this.getValue() <= this.getPlace().getTokens();
	}
	
	public void pull() throws Exception {
		
	}
	
	/* Not enough to determine if 2 arcs are the same
	 * if we consider that only 1 arc can connect a Place to a transition
	 * then it is identified by the transition and the Place
	 */
	public boolean equals(Object o) {
		return o instanceof Arc
				&& ((Arc)o).getPlace().equals(this.getPlace())
				&& ((Arc)o).getValue()==this.getValue()
				&& ((Arc)o).getId()==this.getId();
	}
	
	public String toString() {
		return "Arc: value=" + this.getValue() + " place=" + this.getPlace().getTokens();
	}
}
