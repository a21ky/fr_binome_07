package fr_binome_07;

import java.util.ArrayList;
import java.util.Collections;

public class Transition {
	private ArrayList<Arc> arcs;
	private int id;
	private static int next_id;
	
	public Transition() {
		this.arcs = new ArrayList<Arc>();
		this.id = next_id;
		incrementId();
	}
	
	public Transition(Arc ... arcs) {
		this();
		Collections.addAll(this.getArcs(), arcs);
	}
	
	public Transition(ArrayList<Arc> arcs) {
		this.arcs = arcs;
		this.id = next_id;
		incrementId();
	}
	
	private static void incrementId() {
		next_id += 1;
	}
	
	public static void resetId() {
		next_id = 0;
	}
	
	public int getId() {
		return this.id;
	}
	
	public ArrayList<Arc> getArcs() {
		return this.arcs;
	}
	
	public void addArc(Arc a) {
		this.arcs.add(a);
	}
	
	public Arc removeArc(Arc a) {
		int i = this.getArcs().indexOf(a);
		if (i<=0) {
			return this.getArcs().remove(i);
		}
		return null;
	}
	
	public boolean isPullable() {
		for (Arc a : this.getArcs()) {
			if (!a.isPullable()) {
				return false;
			}
		}
		return true;
	}
	
	public void pull() throws InvalidPullException {
		try {
			for (Arc a : this.getArcs()) {
				a.pull();
			}
		} catch (Exception e) {
			throw new InvalidPullException();
		}
	}
	
	// TO DO: fault: it takes the order of the Arcs into account
	public boolean equals(Object o) {
		return o instanceof Transition
				&& ((Transition)o).getId()==this.getId()
				&& this.getArcs().equals(((Transition)o).getArcs());
	}
}
