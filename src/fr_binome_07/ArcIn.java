package fr_binome_07;

public class ArcIn extends Arc{
	
	public ArcIn(Place p) {
		super(p);
	}
	
	public ArcIn(int value, Place p) {
		super(value, p);
	}
	
	public void pull() throws InvalidPullException {
		try {
			this.getPlace().takeTokens(this.getValue());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new InvalidPullException();
		}
	}
}
