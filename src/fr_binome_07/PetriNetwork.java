package fr_binome_07;

import java.util.ArrayList;
import java.util.HashMap;

public class PetriNetwork {
	private HashMap<Integer, Arc> arcs;
	private HashMap<Integer, Place> places;
	private HashMap<Integer, Transition> transitions;
	//private int placeId;
	private int arcId;
	private int transitionId;
	
	public PetriNetwork() {
		this.arcs = new HashMap<Integer, Arc>();
		this.places = new HashMap<Integer, Place>();
		this.transitions = new HashMap<Integer, Transition>();
	}
	
	public static void resetIds() {
		Place.resetId();
		Transition.resetId();
		Arc.resetId();
	}
	
	public HashMap<Integer, Arc> getArcs() {
		return arcs;
	}

	public HashMap<Integer, Place> getPlaces() {
		return places;
	}

	public HashMap<Integer, Transition> getTransitions() {
		return transitions;
	}
	
	public Arc getArc(int id) {
		return this.getArcs().get(id);
	}
	
	public Place getPlace(int id) {
		return this.getPlaces().get(id);
	}
	
	public Transition getTransition(int id) {
		return this.getTransitions().get(id);
	}

	private void pull(Transition t) throws InvalidPullException {
		t.pull();
	}
	
	public void pull(int transitionId) throws InvalidPullException {
		this.pull(this.transitions.get(transitionId));
	}
	
	public int addPlace(Place p) {
		int id = p.getId();
		this.getPlaces().put(id, p);
		return id;
	}
	
	public ArrayList<Integer> addPlaces(Place ... places) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Place p : places) {
			ids.add(this.addPlace(p));
		}
		return ids;
	}
	
	public int addTransition(Transition t) {
		int id = t.getId();
		this.getTransitions().put(id, t);
		for (Arc a: t.getArcs()) {
			if (!this.getArcs().containsKey(a.getId())) {
				this.addArc(a);
			}
		}
		return id;
	}
	
	public ArrayList<Integer> addTransitions(Transition ... transitions) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Transition t : transitions) {
			ids.add(this.addTransition(t));
		}
		return ids;
	}
	
	public int addArc(Arc a) {
		int id = a.getId();
		this.getArcs().put(id, a);
		if (!this.getPlaces().containsKey(a.getPlace().getId())) {
			this.addPlace(a.getPlace());
		}
		return id;
	}
	
	public int addArc(Arc a, int transitionId) {
		this.transitions.get(transitionId).addArc(a);
		return this.addArc(a);
	}
	
	public ArrayList<Integer> addArcs(Arc ... arcs) {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for (Arc a : arcs) {
			ids.add(this.addArc(a));
		}
		return ids;
	}
	
	public Place removePlace(int id) {
		Place p = this.getPlaces().remove(id);
		HashMap<Integer, Arc> oldArcs = new HashMap<Integer, Arc>(this.getArcs());
		for (Arc a: oldArcs.values()) {
			if (a.getPlace().equals(p)) {
				this.removeArc(a.getId());
			}
		}
		return p;
	}
	
	public Transition removeTransition(int id) {
		Transition t = this.getTransitions().remove(id);
		for(Arc a: t.getArcs()) {
			this.getArcs().remove(a.getId());
			//this.removeArc(a.getId());
		}
		return t;
	}
	
	public Arc removeArc(int id) {
		Arc a = this.getArcs().remove(id);
		for (Transition t: this.getTransitions().values()) {
			t.removeArc(a);
		}
		return a;
	}
	
	public void giveTokens(int id, int tokens) throws Exception {
		this.getPlace(id).giveTokens(tokens);
	}
	
	public void takeTokens(int id, int tokens) throws Exception {
		this.getPlace(id).takeTokens(tokens);
	}
	
	public void setArcValue(int id, int value) {
		this.getArc(id).setValue(value);
	}
	
}
