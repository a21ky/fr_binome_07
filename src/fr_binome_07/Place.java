package fr_binome_07;

public class Place {
	private int tokens;
	private int id;
	private static int next_id; // id of the new Place to create

	public Place() {
		this.tokens = 0;
		this.setId(next_id);
		incrementId();
		
	}
	
	public Place(int tokens) throws Exception {
		if (tokens < 0) {
			throw new Exception("negative tokens");
		}
		this.tokens = tokens;
		this.setId(next_id);
		incrementId();
	}
	
	private static void incrementId() {
		next_id += 1;
	}
	
	public static void resetId() {
		next_id = 0;
	}
	
	public int getId() {
		return this.id;
	}
	
	private void setId(int id) {
		this.id = id;
	}
	
	public int getTokens() {
		return tokens;
	}

	private void setTokens(int tokens) throws Exception {
		if (tokens < 0) {
			throw new Exception("negative tokens");
		}
		this.tokens = tokens;
	}
	
	public void giveTokens(int tokens) throws Exception {
		if (tokens < 0) {
			throw new Exception("negative tokens given");
		}
		this.setTokens(this.getTokens()+tokens);
	}
	
	public void takeTokens(int tokens) throws Exception {
		if (tokens < 0) {
			throw new Exception("negative tokens taken");
		}
		this.setTokens(this.getTokens()-tokens);
	}
	
	public boolean equals(Object o) {
		return o instanceof Place
				&& ((Place)o).getTokens()==this.getTokens()
				&& ((Place)o).getId()==this.getId();
	}
	
	public String toString() {
		return "Place: " + this.getTokens();
	}
}
