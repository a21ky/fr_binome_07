package fr_binome_07;

public class ArcEmpty extends ArcIn{
	
	public ArcEmpty(Place p) {
		super(p);
	}
	
	public boolean isPullable() {
		return this.getPlace().getTokens()>0;
	}
	
	public void pull() throws InvalidPullException {
		try {
			this.getPlace().takeTokens(this.getPlace().getTokens());
		} catch (Exception e) {
			throw new InvalidPullException();
		}
	}
}
